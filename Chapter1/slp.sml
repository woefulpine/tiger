type id = string

datatype binop = Plus | Minus | Times | Div

datatype stm = CompoundStm of stm * stm
	     | AssignStm of id * exp
	     | PrintStm of exp list

     and exp = IdExp of id
	     | NumExp of int
             | OpExp of exp * binop * exp
             | EseqExp of stm * exp

val prog1 = NumExp 5;
val prog2 = OpExp(NumExp 5, Plus, NumExp 3);
val prog3 = PrintStm[NumExp 5];
val prog = 
 CompoundStm(AssignStm("a",OpExp(NumExp 5, Plus, NumExp 3)),
  CompoundStm(AssignStm("b",
      EseqExp(PrintStm[IdExp"a",OpExp(IdExp"a", Minus,NumExp 1)],
           OpExp(NumExp 10, Times, IdExp"a"))),
   PrintStm[IdExp "b"]));


fun length nil = 0
  | length (x::xs) = 1 + length xs;

fun max x y = if x >= y then x else y;
fun maxL (x::xs) = case xs of 
                        nil => x
                      | _ => max x (maxL xs);

 
fun 
  maxargs sttm = case sttm of
                        CompoundStm(stm1, stm2) => max (maxargs stm1)       
                                                         (maxargs stm2)
                      | PrintStm(exps) => max (length exps) 
                                            (maxL (map maxargs2 exps)) 
                      | AssignStm(id, exp1) => maxargs2 exp1 
and
  maxargs2 exxp = case exxp of 
                        IdExp(id) => 0
	                    | NumExp(num) => 0
                      | OpExp(exp1, bop, exp2) => max (maxargs2 exp1)
                                                     (maxargs2 exp2)
                      | EseqExp(stm1, exp1) => max (maxargs stm1) 
                                                   (maxargs2 exp1);

fun extendenv r id v = (id, v)::r;
                      
fun applyenv r id = case r of
                         nil => (print "Error empty env.\n"; raise Match)
                       | ((x,y)::xs) => if (id = x) then (y)
                              else (applyenv xs id);




fun myeval num1 bop num2 = case bop of
                           Plus => num1 + num2
                           | Minus => num1 - num2
                           | Times => num1 * num2
                           | Div   => num1 div num2;




fun interpStm tble sttm = case sttm of
                       CompoundStm(stm1, stm2) => interpStm (interpStm tble stm1) stm2
                                                        
                      | PrintStm(exps) => 
    (print (String.concatWith ",\n" (map (Int.toString) (map (interpExp tble) exps))); tble)

                      | AssignStm(id, exp1) => extendenv tble id exp1

and interpExp tble exxp = case exxp of
	                    NumExp(num) => num
                      | OpExp(exp1, bop, exp2) => (myeval (interpExp tble exp1)
                                                   bop 
                                                   (interpExp tble exp2))
                                                    
                      | EseqExp(stm1, exp1) => interpExp (interpStm tble stm1) exp1

                      | IdExp(id) => interpExp tble (applyenv tble id);







