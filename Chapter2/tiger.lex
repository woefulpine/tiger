type pos = int
type lexresult = Tokens.token

val lineNum = ErrorMsg.lineNum
val linePos = ErrorMsg.linePos

val comlvl : int ref = ref 0
val aStr : string ref = ref ""

fun err(p1,p2) = ErrorMsg.error p1

fun stradd(x, y) = x := !x ^ y

fun asctstr(d) = aStr := !aStr ^ (Char.toString(chr (case d of SOME x => x | NONE => raise Match))); 

fun nextLn pos = ( lineNum := !lineNum+1; linePos := pos :: !linePos)

fun eof () = let val pos = hd(!linePos) in 
                            if !comlvl=0 then Tokens.EOF(pos,pos)
                            else (print("Syntax Error: Comment unclosed.\n"); Tokens.EOF(0,0))

end

%%
%s COMMENT_STATE STRING_STATE;
digit = [0-9];
num = [0-9]+;
alpha =[a-zA-Z];
%%


<INITIAL>{num} => (case Int.fromString(yytext) of 
                                    NONE => (ErrorMsg.error yypos ("No int.\n"); continue())
                                    | SOME(x) => Tokens.INT(x, yypos, yypos+size(yytext))); 
<INITIAL>{alpha}+{num | alpha}* => (Tokens.ID(yytext, yypos, yypos + size(yytext)));


<INITIAL>"/*" => (comlvl := !comlvl + 1; YYBEGIN COMMENT_STATE; continue());
<COMMENT_STATE>"/*" => (comlvl := !comlvl + 1; continue());
<COMMENT_STATE>"*/" => (comlvl := !comlvl - 1; if !comlvl=0 then (YYBEGIN INITIAL) else (); continue());
<COMMENT_STATE>"\n" => (nextLn yypos; continue());
<COMMENT_STATE>. => (continue());


<INITIAL>"\"" => (aStr := ""; YYBEGIN STRING_STATE; continue());
<STRING_STATE>"\\t" => (stradd(aStr, "\t"); continue());
<STRING_STATE>"\\\"" => (stradd(aStr, "\""); continue());
<STRING_STATE>"\\n" => (stradd(aStr, "\n"); continue());
<STRING_STATE>"\\\\" => (stradd(aStr, "\\"); continue());
<STRING_STATE>"\\"{digit}{digit}{digit} => (asctstr(Int.fromString(substring(yytext, 1, size(yytext)-1))); continue());
<STRING_STATE>"\"" => (YYBEGIN INITIAL; Tokens.STRING(!aStr, yypos, yypos+size(!aStr)));
<STRING_STATE>. => (stradd(aStr, yytext); continue());

<INITIAL>"type" => (Tokens.TYPE(yypos, yypos + 4));
<INITIAL>"var" => (Tokens.VAR(yypos, yypos + 3));
<INITIAL>"function" => (Tokens.FUNCTION(yypos, yypos + 8));

<INITIAL>" " => (continue());
<INITIAL>"\n" => (nextLn yypos; continue());
<INITIAL>. => (ErrorMsg.error yypos ("illegal character " ^ yytext); continue());

<INITIAL>



