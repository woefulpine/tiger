structure A = Absyn
structure S = Symbol
structure Tr = Translate
structure E = Env
structure Ty = Types

structure Semant = struct
    exception TypeError

    (*Variable Related Exceptions*)
    exception UndefVar
    exception InvalidVar

    (*Function Related Error*)
    exception UndefFun

    (*TransDecl Error*)
    exception InvalidDecl
    exception InvalidVarInit

    (*Type Decls*)
    type venv = E.envEntry S.table
    type tenv = E.ty S.table

    type aEnv = {venv: venv, tenv: tenv}
    type expty = {exp: Tr.exp, ty: Ty.ty}

    
    						


    (*Processing Variables*)
    fun transVar vEnv tEnv aVar: expty = case aVar of
        A.SimpleVar (sym, _) => (case (S.look(vEnv, sym)) of
                                   SOME (E.VarEntry {ty}) => {exp = (), ty = ty}
                                 | _ => raise UndefVar)
                                        
      | _ => raise InvalidVar

   
    and

    (*Processing Decl List *)
    procDecls vEnv tEnv decs: aEnv = case decs of 
					[] => {venv = vEnv, tenv = tEnv}
				      | (x::xs) => (case (transDec vEnv tEnv x) of
							{venv, tenv} => (procDecls venv tenv xs))
    and

    
    (*Processing Declarations*)
    transDec vEnv tEnv aDec: aEnv = case aDec of
        A.VarDec {name, escape, typ, init, pos} => (case (transExp vEnv tEnv init) of
                      			{exp, ty} => {venv = S.enter(vEnv, name, E.VarEntry {ty = ty}), tenv = tEnv})
                                                           

      | _ => raise InvalidDecl

    and

    (*Processing Expressions.*)
    transExp vEnv tEnv aExp: expty = case aExp of 
        (*type check "basic" types*)
        A.IntExp _ => {exp = (),ty = Ty.INT}

        | A.VarExp var => (transVar vEnv tEnv var)

        | A.StringExp (str, _) => {exp = (), ty = Ty.STRING}

        (*Type check arithmetic ops*)
        | A.OpExp {left, oper, right, pos} => 
                                 let val lefTy = ((transExp vEnv tEnv left) = {exp = (), ty = Ty.INT})
                                     val rhtTy = ((transExp vEnv tEnv right) = {exp = (), ty = Ty.INT})
                                 in
                                     if lefTy andalso rhtTy
                                     then {exp = (), ty = Ty.INT}
                                     else raise TypeError
                                  end

        (*Call exp, unfinished, need to varify args.*)   
        | A.CallExp {func, args, pos} => (case (S.look(vEnv, func)) of
                                              SOME (E.FunEntry {formals, result}) => {exp = (), ty = result}
                                            | _ => raise UndefFun)

        (*Let Exp ...*)
        | A.LetExp {decs, body, pos} => (case (procDecls vEnv tEnv decs) of
					{venv, tenv} => transExp venv tenv body)
					  

        (*type Error for everything else.*)             
        | _ => raise TypeError;



   

    fun transProg aExp = transExp E.base_venv E.base_tenv aExp;
end
