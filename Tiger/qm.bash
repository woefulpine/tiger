#!/usr/bin/bash
TESTS="tests/ -name *.tig"
STOP=""
CATINATE=""

while [ $# -ne 0 ];
do
  case $1 in
    "-s" ) STOP="-s" ;;
    "-c" ) CATINATE="-c" ;;
    "-t" ) shift 
           echo $1      
           TESTS=""$1" -name *.tig" ;;
    "-h" ) printf "This command will run test cases for the tiger compiler. \n"
           printf "The structure test dir must be named tests and may have subdirectories within it.\n"
           printf "The script will run all tests in all subdirectories by default.\n"
           printf "The script must be in the source directory of the tiger compiler, the same dir with the source files.\n\n\n"
           printf "The command is called by running the bash script. The possible options are as follows: \n"
           printf "            -s: The stop directive will pause the test runner in between tests for the purpose of inspecting\n"
           printf "                the output of each test. The script will then prompt the user to continue.\n"
           printf "            -c: The catinate directive will display a the tested src files along side its output from the \n"
           printf "                compiler.\n"
           printf "            -t: The test directive allows the user to specify which subdirectory of tests should be run\n"
           printf "                exclusively. It is applied with an argument which is the path to the relative path to the \n"
           printf "                desired dir.\n\n"
           printf "                   Example: \n"
           printf "                      $> bash ./qm.bash -t tests/controlflowexp/\n"
           printf "                      This command will run the tests in the dir specified and all subdirectories beneath it.\n"
           exit ;;
  esac
  shift
done


for i in $(find $TESTS); do
  printf "$i\n\n"
  echo Control.Print.printDepth := 64\;CM.make \"sources.cm\"\;Main.typCheck \"$i\"\; | sml
  printf "Currently on $i \n\n"

  if [[ -n $CATINATE ]]; then
    printf "$i displayed:\n"
    echo "$(cat "$i")"
  fi
  
  if [[ -n $STOP ]]; then
     printf "\n"
     read -p "Press Any Key To Continue: "
  fi
done
