structure Main = struct
   fun parseFile filename = Parse.parse filename;
   fun typCheck filename = Semant.transProg (parseFile filename);
end
